//
//  main.m
//  Lection2
//
//  Created by Vladislav Grigoriev on 04/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//
#import <stdio.h>

#import <Foundation/Foundation.h>
#import "SPIStarSystem.h"
#import "SPISpaceObject.h"

#import "SPIPlanet.h"
#import "SPIAsteroidField.h"
#import "SPIPlayerSpaceship.h"

#import "SPIStar.h"

int main(int argc, const char * argv[]) {    
    SPIStarSystem *alphaStarSystem = [[SPIStarSystem alloc] initWithName:@"Sirius" age:@(230000000)];
    SPIStarSystem *BStarSystem     = [[SPIStarSystem alloc] initWithName:@"BSystem" age:@(55555555)];
    SPIStarSystem *CStarSystem     = [[SPIStarSystem alloc] initWithName:@"CSystem" age:@(5589855555)];
    SPIStarSystem *DStarSystem     = [[SPIStarSystem alloc] initWithName:@"DSystem" age:@(555595)];
    SPIStarSystem *EStarSystem     = [[SPIStarSystem alloc] initWithName:@"ESystem" age:@(55363555)];
    
    SPIPlanet *vulkanPlanet = [[SPIPlanet alloc] initWithName:@"Vulcan"];
    vulkanPlanet.atmosphere = YES;
    vulkanPlanet.peoplesCount = 325000000;

    SPIAsteroidField *hotaAsteroidField = [[SPIAsteroidField alloc] initWithName:@"Hota"];
    hotaAsteroidField.density = 6000000;
    
    SPIPlanet *gallifreiPlanet = [[SPIPlanet alloc] initWithName:@"Gallifrey"];
    gallifreiPlanet.atmosphere = YES;
    gallifreiPlanet.peoplesCount = 700000000;
    
    SPIPlanet *nabooPlanet = [[SPIPlanet alloc] initWithName:@"Naboo"];
    nabooPlanet.atmosphere = YES;
    nabooPlanet.peoplesCount = 625000000;
    
    SPIPlanet *plutoPlanet = [[SPIPlanet alloc] initWithName:@"Pluto"];
    plutoPlanet.atmosphere = NO;
    
    SPIStar *procylonStar = [[SPIStar alloc] initWithTypeStar:SPITypeStarWhiteDwarf
                                                         name:@"Procylon"
                                                   weightStar:@(856585556.656)];
    
    alphaStarSystem.spaceObjects = @[vulkanPlanet,
                                     hotaAsteroidField,
                                     gallifreiPlanet,
                                     nabooPlanet,
                                     plutoPlanet,
                                     procylonStar];
    
    BStarSystem.spaceObjects = @[vulkanPlanet,
                                 hotaAsteroidField,
                                 gallifreiPlanet,
                                 nabooPlanet,
                                 plutoPlanet,
                                 procylonStar];
    CStarSystem.spaceObjects = @[vulkanPlanet,
                                 hotaAsteroidField,
                                 gallifreiPlanet,
                                 nabooPlanet,
                                 plutoPlanet,
                                 procylonStar];
    
    DStarSystem.spaceObjects = @[vulkanPlanet,
                                 hotaAsteroidField,
                                 gallifreiPlanet,
                                 nabooPlanet,
                                 plutoPlanet,
                                 procylonStar];
    
    EStarSystem.spaceObjects = @[vulkanPlanet,
                                 hotaAsteroidField,
                                 gallifreiPlanet,
                                 nabooPlanet,
                                 plutoPlanet,
                                 procylonStar];
    
    NSDictionary* waysStarSystem = [NSDictionary dictionaryWithObjectsAndKeys:
                                    @[BStarSystem,  CStarSystem],       alphaStarSystem.name,
                                    @[alphaStarSystem, DStarSystem],    BStarSystem.name,
                                    @[alphaStarSystem, EStarSystem],    CStarSystem.name,
                                    @[BStarSystem,     EStarSystem],    DStarSystem.name,
                                    @[CStarSystem,     DStarSystem],    EStarSystem.name,
                                    nil];
   
    SPIPlayerSpaceship *spaceship = [[SPIPlayerSpaceship alloc] initWithName:@"Falcon"];
    [spaceship loadStarSystem:alphaStarSystem];
    [spaceship loadStarSystemWays:waysStarSystem];
    
    BOOL play = YES;
    
    NSMutableArray *gameObjects = [[NSMutableArray alloc] init];
    [gameObjects addObjectsFromArray:alphaStarSystem.spaceObjects];
    [gameObjects addObject:spaceship];

    while (play) {
        SPIPlayerSpaceshipResponse response = [spaceship waitForCommand];
        
        if (response == SPIPlayerSpaceshipResponseExit) {
            play = NO;
            continue;
        }
        
        for (id<SPIGameObject> gameObject in gameObjects) {
            [gameObject nextTurn];
        }
    }
    return 0;
}


