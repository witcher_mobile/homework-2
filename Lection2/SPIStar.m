//
//  SPIStar.m
//  Lection2
//
//  Created by Alexander Mihailov on 11/6/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPIStar.h"

@implementation SPIStar

+ (NSString *)typeStarStringForType:(SPITypeStar)type {
    switch (type) {
        case SPITypeStarWhiteDwarf:
            return @"White dwarf";
            
        case SPITypeStarBrownDwarf:
            return @"Brown dwarf";
            
        case SPISypeStarRedGigand:
            return @"Red gigand";
            
        default:
            return @"Unknown";
    }
}


- (instancetype)initWithTypeStar:(SPITypeStar)type
                            name:(NSString *) name
                      weightStar:(NSNumber *) weight{
    
    self = [super initWithType:SPISpaceObjectTypeStar name:name];
    
    if(self){
        _typeStar   = type;
        _weightStar = weight;
    }
    
    return self;
}

- (void) nextTurn{
    [super nextTurn];
    /// ?????
}

- (NSString *) description{
    return [NSString stringWithFormat:@"\nStar: %@,\nType star: %@ \nWeight star: %@", self.name,
            [SPIStar typeStarStringForType:self.typeStar], self.weightStar];
}

@end

