//
//  SPIStar.h
//  Lection2
//
//  Created by Alexander Mihailov on 11/6/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPISpaceObject.h"

// Перечисление типов звезд.
typedef NS_ENUM (NSInteger, SPITypeStar){
    SPITypeStarBrownDwarf,  // Коричневый карлик.
    SPITypeStarWhiteDwarf,  // Белый карлик.
    SPISypeStarRedGigand,   // Красный гиганд.
};

// Класс звезды.
@interface SPIStar : SPISpaceObject

@property(nonatomic, assign, readonly)  SPITypeStar typeStar;       // Тип звезды.
@property(nonatomic, strong)            NSNumber    *weightStar;    // Масса звезды.

// Конструктор с параметрами: тип звезды, ее имя и ее масса.
- (instancetype)initWithTypeStar:(SPITypeStar)type
                           name:(NSString *) name
                     weightStar:(NSNumber *) weight;

@end
