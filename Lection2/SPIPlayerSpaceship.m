//
//  PlayerSpaceship.m
//  Lection2
//
//  Created by Vladislav Grigoriev on 17/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPIPlayerSpaceship.h"

@implementation SPIPlayerSpaceship

- (instancetype)initWithName:(NSString *)name {
    self = [super init];
    if (self) {
        _name = name;
    }
    return self;
}

- (void)loadStarSystem:(SPIStarSystem *)starSystem {
    _starSystem = starSystem;
    _currentSpaceObject = _starSystem.spaceObjects.firstObject;
}

- (void)loadStarSystemWays:(NSDictionary *) ways{
    _waysStarsSystems = ways;
}

- (SPIPlayerSpaceshipResponse)waitForCommand {
    char command[255];

    printf("\n%s", [[self availableCommands] cStringUsingEncoding:NSUTF8StringEncoding]);
    printf("\n%s ",[@"Input command:" cStringUsingEncoding:NSUTF8StringEncoding]);
    
    fgets(command, 255, stdin);
    int commandNumber = atoi(command);
    self.nextCommand = commandNumber;
    
    return self.nextCommand == SPIPlayerSpaceshipCommandExit ? SPIPlayerSpaceshipResponseExit : SPIPlayerSpaceshipResponseOK;
}

- (NSInteger) choiceStarSystemTravel:(NSArray*) array{
    for(NSUInteger i = 0; i < [array count]; ++i){
        SPIStarSystem *value = (SPIStarSystem*)array[i];
        printf("%s\n", [[NSString stringWithFormat:@"%lu - Fly to %@", (unsigned long)i, value.name] cStringUsingEncoding:NSUTF8StringEncoding]);
    }
    
    char command[255];
    fgets(command, 255, stdin);
    return atoi(command);
}

- (void)nextTurn {
    
    switch (self.nextCommand) {
        case SPIPlayerSpaceshipCommandFlyToPreviousObject: {
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if (currentSpaceObjectIndex > 0) {
                self.currentSpaceObject = [self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex - 1];
            }
            break;
        }
            
        case SPIPlayerSpaceshipCommandExploreCurrentObject: {
            printf("%s\n", [[self.currentSpaceObject description] cStringUsingEncoding:NSUTF8StringEncoding]);
            break;
        }
            
        case SPIPlayerSpaceshipCommandFlyToNextObject: {
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if (currentSpaceObjectIndex < (self.starSystem.spaceObjects.count - 1)) {
                self.currentSpaceObject = [self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex + 1];
            }
            break;
        }
        case SPIPlayerSpaceshipCommandFlyToStarSystem:{
            NSArray *array = [self.waysStarsSystems objectForKey:self.starSystem.name];
            NSInteger count = [array count];
            NSInteger index = -1;
            
            while(index < 0 || index > count - 1)
                index = [self choiceStarSystemTravel:array];
            
            [self loadStarSystem:array[index]];
            break;
        }
            
        default:
            break;
    }
    self.nextCommand = 0;
}

- (NSString *)availableCommands {
    if (self.starSystem) {
        NSMutableArray *mutableDescriptions = [[NSMutableArray alloc] init];
        for (NSInteger command = SPIPlayerSpaceshipCommandFlyToPreviousObject; command <= SPIPlayerSpaceshipCommandFlyToStarSystem; command++) {
            NSString *description = [self descriptionForCommand:command];
            if (description) {
                [mutableDescriptions addObject:[NSString stringWithFormat:@"%ld. %@", (long)command, description]];
            }
        }
        
        NSString *description = [self descriptionForCommand:SPIPlayerSpaceshipCommandExit];
        if (description) {
            [mutableDescriptions addObject:[NSString stringWithFormat:@"%ld. %@", (long)SPIPlayerSpaceshipCommandExit, description]];
        }
        
        return [mutableDescriptions componentsJoinedByString:@"\n"];
    }
    return nil;
}

- (NSString *)descriptionForCommand:(SPIPlayerSpaceshipCommand)command {
    NSString *commandDescription = nil;
    switch (command) {
        case SPIPlayerSpaceshipCommandFlyToPreviousObject: {
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if (currentSpaceObjectIndex > 0) {
                commandDescription = [NSString stringWithFormat:@"Fly to %@", [[self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex - 1] title]];
            }
            break;
        }
            
        case SPIPlayerSpaceshipCommandExploreCurrentObject: {
            commandDescription = [NSString stringWithFormat:@"Explore %@", [self.currentSpaceObject title]];
            break;
        }
            
        case SPIPlayerSpaceshipCommandFlyToNextObject: {
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if (currentSpaceObjectIndex < (self.starSystem.spaceObjects.count - 1)) {
                commandDescription = [NSString stringWithFormat:@"Fly to %@", [[self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex + 1] title]];
            }
            break;
        }
            
        case SPIPlayerSpaceshipCommandFlyToStarSystem: {
            if(!self.waysStarsSystems) break;
            commandDescription = @"Fly to ";
            NSArray *array = [self.waysStarsSystems objectForKey:self.starSystem.name];
            for(SPIStarSystem *value in array){
               commandDescription = [commandDescription stringByAppendingFormat:@"%@; ", value.name];
            }
            break;
        }
            
        case  SPIPlayerSpaceshipCommandExit: {
            commandDescription = [NSString stringWithFormat:@""];
            break;
        }
            
        default:
            break;
    }
    return commandDescription;
}


@end
